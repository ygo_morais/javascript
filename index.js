//#TYPE NAMBER

var nota = 10;

nota.toFixed(2);  //10.00
nota.toString(); //"10"

//----------------

//#TYPE STRING

var nome = "AgileCode";

nome.charAt(2); //i     	//valor na possição
nome.charCodeAt(0); //65  	//na possição 0 (A) retorna o seu codigo ASC
nome.concat("!"); //AgileCode!
nome.indexOf('e'); //4    	//busca a possicao do primeiro "e"
nome.replace('Code', '!'); 	//Agile!
nome.split("e");//['Agil','Cod','']

nome = "Pedro Silva";
nome.match("Silva"); //6   //busca a possicao da string Silva
nome.substring(0,5); //Pedro  //
nome.substring(5); 	 // Silva
nome.trim(); //remove os espaços iniciais e finais da string

//----------------


//#FUNÇÕES

// funcao de primeira classe: pode ser atribuida a uma variavel e ser passada por parametro
// funcao declaration: nesse caso não acontece erro na invocação
console.log(soma(2,5));
function soma(a,b){
	return a+b;
}

//----------------

// funcao expression: há erro no codigo abaixo: invocar antes de declarar a funcao
console.log(soma(2,5));
var soma = function(){
	return a+b;
}

//----------------


//passando funcao e objeto como parametro
var produto = {nome:"sapato", preco:2}
var formulaA = function(preco){
	return preco*0.2
};
var calcImpos = function(produto, formImpost){
	return produto.preco + formImpost(produto.preco);
}
console.log(calcImpos(produto, formulaA));

//----------------

//retornando funcao
var helloword = function(){
	return function(){
		return "hello";
	};
};
console.log(helloword()());//"hello"

//----------------

//arguments: retornar todos os argumentos que foram enviados para essa funcao
var getIdade = function(extra){
	console.log(arguments);
}

//----------------

//funcao fabrica: funcao para criar objetos
var criarPessoa = function(nome, idade){
	return { //retornando um objeto
		nome: nome,
		idade: idade
	}
}
console.log(criarPessoa("ygo", 22));
console.log(criarPessoa("ana", 21)); //{ nome: 'ygo', idade: 22 } { nome: 'ana', idade: 21 }*/

//----------------

//funcao construtora: começa com letra maiúscula, para chama-la deve-se utilizar o NEW
var CriarPessoa = function(nome, idade){
	this.nome = nome;   // os this, referenciam ao novo objeto
	this.idade = idade;
}
console.log(new CriarPessoa("maria", 20));

//----------------

//closures (encapsulamento, fechamento): funções internas tem acesso a variaveis externas

//retornando uma funcao
//funcao de primeira classe
var helloword = function(){
	var mensagem = "hello";
	return function(){
		return mensagem;  //closures 
	};
};
console.log(helloword()()); //out: hello*/

//----------------

//encapsulando funções em objetos
//nesse caso, poderá haver problema de integridade
var counter = {
	value: 0,
	add: function(){
		return ++this.value;//refere-se ao value da linha anterios   //closures 
	}
}

console.log(counter.value);	//0  //problema de integridade
console.log(counter.add());	//1
console.log(counter.add());	//2
console.log(counter.add());	//3*/

//----------------

//encapsulando funções dentro de outras funções
//factory function
var createCounter = function(){
	var value = 0;
	return { //tudo que está no return se tornará publico 
		add: function(){
			return  ++value;  //closures  //não há this, pos o value não faz parte do objeto
		},
		reset: function(){
			return value = 0;
		}
	};
};

var counter = createCounter();
console.log(counter.value); //undefined
console.log(counter.add()); //1
console.log(counter.add()); //2
console.log(counter.add()); //3
counter.reset();
console.log(counter.add()); //1

//----------------

//module pattern
//com o uso desse modulo a linha: "var counter = createCounter();" pode ser desconsiderada
var counter = (function(){
	var _value = 0;
	var _add = function(){
		return  ++_value;  //closures  //não há this, pos o value não faz parte do objeto
	};
	var _reset =  function(){
		_value = 0;
	};
	return { //tudo que está no return se tornará publico 
		add: _add,
		reset: _reset	
	};
})();

console.log(counter.value); //undefined   //encapsulamento, o atributo será "privado"
console.log(counter.add()); //1
console.log(counter.add()); //2
console.log(counter.add()); //3
counter.reset();
console.log(counter.add()); //1*/

//----------------


//#ARRAY

var carros = new Array(10); //criando array com limitador
console.log(carros); //[ , , , , , , , , ,  ]

//----------------

var carros = ["ka", "corsa", "palio"];

carros.valueOf(); 			//['ka', 'corsa', 'palio']
carros.toString(); 			//'ka,corsa,palio';
carros.length;  //3    		//length não é uma funcao e sim, uma propriedade
carros.push("gol") 			//inserindo novo elemento no final do array
carros.unshift("gol"); 		//inserindo elementono no inicio
carros.pop(); 				//retorna o ultimo elemento e o retira do array
carros.shift(); 				//retorna o primeiro elemento e o retira do array
carros.indexOf("corsa");		//buscando a posição do elemento 

carros.splice(1,1);			//param: (posição, qtd_elementos_removidos) //retirando elementos do vetor pelo indice
carros.splice(1,1,"sonic");	//param: (posição, qtd_elementos_removidos, 'substituto') //substituindo elemento
carros.splice(1,0,"sonic");	//nesse caso, nenhum elemento será removido, o "sonic" será adicionado na posição 1

carros.forEach(function(elemento, index, vetor){ //percorrendo vetor
	console.log(elemento+" - "+index+" - "+vetor); //ka - 0 - ka,corsa,palio
});

//----------------

var carros = [];
carros[0] = {marca: "ford", modelo: "ka", preco: 20};
carros[1] = {marca: "chevrolet", modelo: "corsa", preco: 30};
carros[2] = {marca: "fiat", modelo: "palio", preco: 10};

//----------------

//#filter
var carrosFil = carros.filter(function(elemento){ //percorre o array e retorna os elemento filtrados (que satisfazem a expressão boolean)
	return elemento.marca === "ford";
});

console.log(carrosFil); //[{marca: 'ford', modelo: 'ka'}];

//----------------

//#every: verifica se todos os elementos do vetor satisfazem a expressao boolean
var flag = carros.every(function(elemento){
	return elemento.marca === "ford";
});

console.log(flag); //false

//----------------

//#some: verifica se há pelo menos um elemento que satisfasa a expressão boolean
var flag = carros.some(function(elemento){
	return elemento.marca === "ford";
});

console.log(flag); //true

//----------------

//#map :criando um novo array derivado de um array

var novoCarros = carros.map(function(elemento){
	return elemento.marca;
});

console.log(novoCarros); //['ford', 'chevrolet', 'fiat']

//----------------


//#reduce: somando ou concatenando todos os elemento de um vetor
var totalPreco = carros.reduce(function(prev, cur){ //prev: elemento anterior, cur: elemento atual
	return prev + cur.preco; //somando todos os precos
}, 0); //valor inicial do total final, utilizado para evitar o undefined da variavel prev da primeira interação

console.log(totalPreco); //60

//----------------

//#juntando arrays
var carros = ['ka', 'corsa', 'palio'];
var motos = ['honda', 'yamaha'];

var veiculos = carros.concat(motos);
console.log(veiculos); //['ka', 'corsa', 'palio', 'honda', 'yamaha']

//----------------

var carros = ['ka', 'corsa', 'palio', 'gol'];

//#slice: fatiando o array em arrays menores
carros.slice(0,2); 	//['ka', 'corsa']
carros.slice(1,3); 	//['corsa','palio']
carros.slice(2);	//['palio', 'gol']

//----------------

//#reverse: revertendo a ordem do vetor original
carros.reverse();
console.log(carros); //[ 'gol', 'palio', 'corsa', 'ka' ]

//----------------

//#sort: ordenando vetor por...
carros.sort(); //...ordem alfabetica
console.log(carros); //[ 'corsa', 'gol', 'ka', 'palio' ]

//----------------

//#join: //juntando o elementos do array colocando uma substring entre eles
var carrosJuntos = carros.join(";") 
console.log(carrosJuntos);  //ka;corsa;palio;gol


//----------------

//#EXPRESSÕES REGULARES: são estruturas formadas por uma sequencia de caracteres que especificam um padrao

var regExp = /<expressao>/; 				 //criando expressões regulares - forma 1
var regExp = new RegExp("<expressao>");  	 //criando expressões regulares - forma 2

//----------------

//#telefone:  (9999-9999);
var regExp = /9999-9999/;
var telefone = "9999-9999";
console.log(regExp.exec(telefone)); //exec: retorna detalhes de inserção e resultado
console.log(regExp.test(telefone)); //true     //test: retorno boolean, verifica se o valor é equivalente a expressao regular

//----------------

//telefone: (48) (9999-9999)
var regExp = /\(48\) 9999-9999/;  //    \:utilizado para "escapar" caracteres especiais
var telefone = "(48) 9999-9999";
console.log(regExp.test(telefone)); //true     //test: retorno boolean, verifica se o valor é equivalente a expressao regular

//----------------

//telefone:  fazer com que não tenha caracteres antes e depois do telefone
//caso1
var regExp = /\(48\) 9999-9999/;
var telefone = "aaaa(48) 9999-9999aaaa";
console.log(regExp.test(telefone)); //true   

//caso:2
var regExp = /^\(48\) 9999-9999$/;  //  ^:deve comecar com abrir parentese   $:deve finalizar com 9
var telefone = "aaaa(48) 9999-9999aaaa";
console.log(regExp.test(telefone)); //false   

//caso:3
var regExp = /^\(48\) 9999-9999$/;  //  ^:deve comecar com abrir parentese   $:deve finalizar com 9
var telefone = "(48) 9999-9999";
console.log(regExp.test(telefone)); //true

//----------------

//grupos de caracteres
/*[abc] - aceita somente os caractere dentro do grupo a,b ou c
[^abc] - não aceita somente os caractere dentro do grupo a,b ou c
[0-9] - aceita qualquer caractere entre 0 e 9
[^0-9] - não aceita os caractere entre 0 e 9*/

//grupo de caracteres sem intervalo
//nesse caso, busca "," e ";"
'1222,2313;4321'.split(/[,;]/) // ["1222", "2313", "4321"]

//aceitar qualquer telefone
var regExp = /^\([0-9][0-9]\) [0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]$/;  //  ^:deve comecar com abrir parentese   $:deve finalizar com 9
var telefone = "(09) 1234-5678";
console.log(regExp.test(telefone)); //true

//----------------

//quantificadores: evitando escrever muitos grupos juntos (exemplo anterior)
/*{n} 	- quantifica um numero especifico
{n,} 	- quantifica um numero minimo
{n,m} 	- quantifica um numero minimo e maximo*/


// utilizando quantificadores
var regExp = /^\([0-9]{2}\) [0-9]{4}-[0-9]{4}$/; //algo como [0-9]*2
var telefone = "(09) 1234-5678";
console.log(regExp.test(telefone)); //true

//----------------

//telefone: fazer com que aceite 8 ou 9 digitos
var regExp = /^\([0-9]{2}\) [0-9]{4,5}-[0-9]{4}$/;
var telefone = "(09) 91234-5678";
console.log(regExp.test(telefone)); //true

var telefone = "(09) 1234-5678";
console.log(regExp.test(telefone)); //true

//----------------

//quantificadores em caracteres
/*
	? - Zero ou um, ou seja, opcional
	* - zero ou mais
	+ - um ou mais, ou seja obrigatorio
*/

//telefone: fazer com que o hifem possa ser opcional
var regExp = /^\([0-9]{2}\) [0-9]{4,5}-?[0-9]{4}$/;
var telefone = "(09) 91234-5678";
console.log(regExp.test(telefone)); //true

var telefone = "(09) 912345678";
console.log(regExp.test(telefone)); //true

//----------------

/*
matacaracteres
	. - representa qualquer caractere
	\w - representa o conjunto [a-zA-Z0-9]
	\W - representa o conjunto [^a-zA-Z0-9]
	\d - representa o conjunto [0-9]
	\D - representa o conjunto [^0-9]
	\s - representa um espaço em branco
	\S - representa um não espaco em branco
	\n - representa uma quebra de linha
	\t - representa um tab
*/

//simpleficando a expressao
var regExp = /^\(\d{2}\)\s\d{4,5}-?\d{4}$/; //substituir [0-9] por \d e espaco por \s
var telefone = "(09) 91234-5678";
console.log(regExp.test(telefone)); //true

//----------------


//STRING API
/*
	match - executa uma busca na string e retorna o array contendo os dados encontrados
	split - divide a string com base em outra string ou expressao regular
	replace - substitui partes da string com base em outra string ou expressao regular
*/ 


//modificadores
/*
	i - case-insensitive matching, aceita caracteres maiusculas e minusculas
	g - global matching, quando encontrar o primeiro continue a procurar por outros
	m - -multiline matching, quebra de linha
*/

//----------------

//buscando telefones em uma string maior e armazenando em um vetor
var regExp = /\(\d{2}\)\s\d{4,5}-?\d{4}/g;// g:global matching
var telefone = "(98) 1234-1212 f111qeqw (11) 22321212Xdasdsaw";
console.log(telefone.match(regExp));   //[ '(98) 1234-1212', '(11) 22321212' ]

//----------------

//substituindo caracteres com expressoes regulares
var regExp = /\(\d{2}\)\s\d{4,5}-?\d{4}/g;// g:global matching

var telefone = "(98) 1234-1212 f111qeqw (11) 22321212Xdasdsaw";
console.log(telefone.replace(regExp, "(XX) XXXX-XXXX"));   //(XX) XXXX-XXXX f111qeqw (XX) XXXX-XXXXXdasdsaw

//----------------


//#OPERADORES
//verificar o tipo da variavel
//typeof 10; //number
//typeof 'aaa'; //string

//ternario
var idade = 18;
var resultado = (idade >=10) ? "verdadeiro" : "falso";
console.log(resultado);

//----------------

//#TRATAMENTO DE ERROS
//um objeto somente para conter as mensagens de erros
var TextError = function(mensagem, nome){
	this.mensagem = mensagem;
	this.nome = "Erro no programa";
}

var lerTexto = function(texto){
	if(!texto) throw new TextError("digite um texto!");
	if(typeof texto !== "string") throw new TextError("somente numeros!");
	return texto;
}

try{
	console.log(lerTexto("asdasdadadadsda"));
}catch(e){
	console.log("erro: "+e.mensagem+" - nome: "+e.nome);
}


try{
	console.log(lerTexto());
}catch(e){
	console.log("erro: "+e.mensagem+" - nome: "+e.nome);
}

try{
	console.log(lerTexto(1312312));
}catch(e){
	console.log("erro: "+e.mensagem+" - nome: "+e.nome);
}

//----------------


//#HERANÇA
var homem = {
	sexo: "masculino"
};

var joao = {
	nome: "joao",
	idade: 18
}
//herança - metodo 1
Object.setPrototypeOf(joao, homem);  

//herança - metodo 2
var pedro = Object.create(homem);
pedro.nome = "pedro";
pedro.idade = 18;


console.log(joao);
console.log(joao.sexo);
console.log(pedro);
console.log(pedro.sexo);

for(var propriedade in joao){
	console.log(propriedade); //nome  \n idade  \n sexo
}

//----------------

//invocando função pelo CALL
var Homem = function(nome, idade){
	this.nome = nome;
	this.idade = idade;
}

var pedro = {};
Homem.call(pedro, "pedro", 18); //invocando funcao com CALL
console.log(pedro);  //{ nome: 'pedro', idade: 18 }